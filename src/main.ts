import express from 'express';
import path from 'path';
import debug from 'debug';
import bodyParser from 'body-parser';

import config from './config/config';
import Database from './helper/database';
import userRouter from './routes/user.router';
import adminRouter from './routes/admin.router';

debug.enable('store:*');

const app = express();

// Configure Express to use EJS
// app.set('views', path.join(config.GetRootPath(), 'views'));
// app.set('view engine', 'ejs');
/**
 * Parse incoming body data to JSON object
 */
app.use(bodyParser.json());
/**
 * Enable interacting between hosts running on different ports or/and IP's (CORS error)
 */
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

/**
 * Routes for manage Users
 */
app.use('/api', userRouter);
app.use('/api/admin', adminRouter);

/**
 * Middleware to handle URL's not found
 */
app.use('/', (req, res, next) => {
    const D = debug('store:error');
    const data = {
        status: 404,
        message: 'Resource not found. URL not found',
        url: `${req.method} ${req.url}`
    };
    D(`URL not found ${data.url}`);
    res.status(200).json(data);
});

/**
 * Run the server and listen for incomming connections
 */
app.listen(config.GetPort(), () => {
    const D = debug('store:listen');
    const db = new Database(
        config.GetDatabaseUser(),
        config.GetDatabasePassword(),
        config.GetDatabaseName()
    );
    db.connect();
    D(`Server started on port: ${config.GetPort()}`);
});
