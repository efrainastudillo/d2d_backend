import dotenv from 'dotenv';
import debug from 'debug';

const LOG = debug('store:config');
debug.enable('store:*');

export class Config {
    private env: any;

    constructor() {
        LOG('Config new instance...');
        this.env = dotenv.config();
        if (this.env.error) {
            LOG('Error parsing ENV file');
        }
    }

    // eslint-disable-next-line class-methods-use-this
    GetPort() : number {
        if (process.env.PORT === undefined) {
            LOG('PORT is undefined. Check the port in your environment file');
            throw new Error('PORT is undefined. Check the port in your environment file');
        }
        return +process.env.PORT;
    }

    /**
     * Return the Root path from environemnt variable
     * @returns Root path as string
     */
    // eslint-disable-next-line class-methods-use-this
    GetRootPath() : string {
        if (process.env.ROOT_URL === undefined) {
            LOG('ROOT_URL is undefined. Check the path in your environemt file');
            throw new Error('ROOT_URL is undefined. Check the path in your environemt file');
        }
        return process.env.ROOT_URL;
    }

    // eslint-disable-next-line class-methods-use-this
    GetDatabasePassword(): string {
        if (process.env.DBPASSWORD === undefined) {
            LOG('Database Password is not defined in ENV file.');
            throw new Error('Database Password is not defined in ENV file.');
        }
        return process.env.DBPASSWORD;
    }

    // eslint-disable-next-line class-methods-use-this
    GetDatabaseName(): string {
        if (process.env.DBNAME === undefined) {
            LOG('DBNAME is not defined in ENV file.');
            throw new Error('DBNAME is not defined in ENV file.');
        }
        return process.env.DBNAME;
    }

    // eslint-disable-next-line class-methods-use-this
    GetDatabaseUser(): string {
        if (process.env.DBUSERNAME === undefined) {
            LOG('Database Password is not defined in ENV file.');
            throw new Error('Database Password is not defined in ENV file.');
        }
        return process.env.DBUSERNAME;
    }

    // eslint-disable-next-line class-methods-use-this
    getJwtPrivateKey(): string {
        if (process.env.JWT_PRIVATE_KEY === undefined) {
            LOG('JWT_PRIVATE_KEY is not defined in ENV file');
            throw new Error('JWT_PRIVATE_KEY is not defined in ENV file');
        }
        return process.env.JWT_PRIVATE_KEY;
    }
}
const config = new Config();
export default config;
