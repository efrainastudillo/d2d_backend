import mongoose, { Mongoose, Schema, Document } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
// import  from './schemas/user.schema';

// variables names should be the same as in the SChema
export interface IUser extends Document {
    username: string;
    email: string;
    firstname: string;
    password: string;
}

const UserSchema = new Schema({
    username: { type: String, required: true },
    email: { type: String, required: true },
    firstname: { type: String, required: false },
    password: { type: String, required: true }
});
UserSchema.plugin(uniqueValidator);
export default mongoose.model<IUser>('User', UserSchema);
