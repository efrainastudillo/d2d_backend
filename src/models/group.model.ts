import mongoose, { Schema, Document } from 'mongoose';

export interface Group extends Document {
    name: string;
}

const GroupSchema = new Schema({
    name: { type: String, required: true }
});
// UserSchema.plugin(uniqueValidator);
export default mongoose.model<Group>('Group', GroupSchema);
