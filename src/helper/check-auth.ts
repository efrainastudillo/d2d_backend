import jwt from 'jsonwebtoken';
import e, { NextFunction } from 'express';
import config from '../config/config';
import { ReturnCode } from './return-codes';

// eslint-disable-next-line consistent-return
const authCheck = (req: e.Request, res: e.Response, next: NextFunction) => {
    const result = {
        status: 0,
        message: ''
    };

    try {
        if (req.headers.authorization === undefined) {
            result.status = 402;
            result.message = 'Authorization header is not present in current request!';
            return res.status(200).json(result);
        }
        const token = req.headers.authorization!.split(' ');
        // token must contain 2 elements after splitted. Because of the convention
        // of using 'Bearer ...' at the beggining of the token
        if (token.length <= 1) {
            result.status = 403;
            result.message = 'Token is not parsed correctly!';
            return res.status(ReturnCode.OK).json(result);
        }
        if (jwt.verify(token[1], config.getJwtPrivateKey())) {
            next();
        }
    } catch (err) {
        return res.status(ReturnCode.OK).json(
            { status: 401, message: `Token unrecognized! Unable to access to this Site. - ${err}` }
        );
    }
};

export default authCheck;
