import path from 'path';
import { Shipment } from './shipment';
import readDataFromFile from './readfile';

export default class FedexShipment implements Shipment {
    url: string;
    body: JSON | undefined;

    constructor() {
        this.url = 'https://wsbeta.fedex.com:443/web-services';
        this.body = undefined;
        try {
            const filename = path.join(__dirname, '../', 'currier-api-request/rating_fedex_xml.json');
            this.body = JSON.parse(readDataFromFile(filename));
        } catch (error) {
            console.log(`Error reading a file ${error}`);
        }
    }
    
    async rateShipment() : Promise<any> {
        throw new Error('Method not implmeented yet');
    }
}
