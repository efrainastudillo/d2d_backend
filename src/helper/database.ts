import { MongoClient, MongoCallback, Db } from 'mongodb';
import mongoose from 'mongoose';
import debug from 'debug';

const LOG = debug('store:database');

export default class Database {
    db!: Db;

    uri: string;

    options = {
        useNewUrlParser: true,
        useUnifiedTopology: true
    };

    constructor(private username?: string, private password?: string, private dbname?: string) {
        // this.uri = `mongodb+srv://${username}:qvcB6WeGVKLdJSNT@cluster0-0ys9x.mongodb.net/test?retryWrites=true&w=majority`;
        if (dbname === undefined) {
            this.uri = 'mongodb://127.0.0.1:27017/test?compressors=zlib&gssapiServiceName=mongodb';
        } else {
            this.uri = `mongodb://127.0.0.1:27017/${dbname}?compressors=zlib&gssapiServiceName=mongodb`;
        }
        // this.client = new MongoClient(this.uri, optionsDB);
    }

    // OnConnect(callback: MongoCallback<MongoClient>) : void {
    //     this.client.connect(callback);
    //     this.db = this.client.db(this.dbname);
    // }

    connect() : void {
        mongoose.connect(this.uri, this.options).then((data) => {
            LOG('Database connected!!!');
        }).catch((err) => {
            LOG(`Error connecting Database - ${err}`);
        });
    }

    shutdown() : void {
        mongoose.disconnect((err) => {
            if (err) {
                console.log(`Error disconnecting Mongo database ${err}`); 
            }
        });
    }
}
