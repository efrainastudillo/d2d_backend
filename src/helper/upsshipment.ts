import { Shipment } from './shipment';
import readDataFromFile from './readfile';

export default class UPSShipment implements Shipment {
    url: string;
    body: any | undefined;

    constructor() {
        this.url = 'https://onlinetools.ups.com/ship/v1/rating/Rate';
        this.body = undefined;
        try {
            const path = `${__dirname}/../currier-api-request/rating_ups_ground_request.json`;
            const data = readDataFromFile(path);
            this.body = JSON.parse(data);
            // this should be removed later!!!
            console.log(`This is the body of the data ${this.body['RateRequest']['Request']['SubVersion']}`);
        } catch (error) {
            console.log(`Error reading a file ${error}`);
        }
    }

    setParams(_params: any) : void {
        if (this.body === undefined || _params === undefined) {
            throw new Error('Parameter is undefined');
        }

        const keys = Object.keys(this.body);
        console.log('Printing all keys....', keys);
        
        keys.forEach((item, index) => {
            console.log(item); // first item is [request]
            console.log(index);
        });
    }

    async rateShipment(_params: any): Promise<any> {
        // this.body
        return [];
    }
}
