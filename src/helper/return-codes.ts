// eslint-disable-next-line import/prefer-default-export
export enum ReturnCode { 
    OK = 200,
    ERROR = 201,
    RESOURCE_NOT_FOUND = 400
}

export enum AppReturnCode {
    OK = 1,
    ERROR = 2
}
