import fs from 'fs';

/**
 * Read a file.
 * @param _path The path of the file to be read. This path should be absolute.
 * @returns Buffer of the file read.
 * @throws an Error if something wrong happens. i.e path doens't exist...
 */
const loadDataFromFile = (_path: string) : string => {
    if (!fs.existsSync(_path)) { // if doesn't exist
        console.log(`Error file doesn't exist: ${_path}`);
        throw new Error(`Error file doesn't exist: ${_path}`);
    }
    try {
        const data = fs.readFileSync(_path);
        return `${data}`; //
    } catch (err) {
        console.log(`Error reading a file: ${_path} - ${err}`);
        throw new Error(`Error reading a file: ${_path} - ${err}`);
    }
};

export default loadDataFromFile;
