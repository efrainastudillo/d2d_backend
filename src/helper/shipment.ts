
export interface Shipment {
    url: string; // external API URL to request a service
    body: {} | undefined; // the request data to sent
    /**
     * \return json object with the result of a Rating
     */
    rateShipment(params: any): Promise<any>;

}
