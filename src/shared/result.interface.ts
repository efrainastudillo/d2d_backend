export default interface AuthResult {
    status: number;
    message: string;
    id: string;
    token: string;
// eslint-disable-next-line semi
}
