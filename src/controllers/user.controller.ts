import { Request, Response } from 'express';
import csv2json from 'csvtojson';
import https from 'https';

import debug from 'debug';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

import config from '../config/config';

import User, { IUser } from '../models/user.model';
import AuthResult from '../shared/result.interface';
import { ReturnCode } from '../helper/return-codes';

const LOG = debug('store:user-controller');

export default class UserController {
    /**
     * Return the user identified by ID 
     * @param req 
     * @param res 
     */
    static async getUserById(req: Request, res: Response) {
        const { id } = req.params;
        try {
            const user = await User.findById(id);
            res.status(ReturnCode.OK).json({
                status: ReturnCode.OK,
                message: 'User found.',
                user
            });
        } catch (error) {
            res.json(ReturnCode.ERROR).json({ 
                status: ReturnCode.ERROR,
                message: `Error retrieving user by id: ${id} - ${error}` 
            });
        }
    }
    /**
     * Return users from database
     * @param req Request object from client
     * @param res Response object to be sent to client
     */
    static async getUsers(req: Request, res: Response) {
        LOG(`${req.method} ${req.url}`);
        const { body, method, url } = req;
        console.log(`${method} ${url} ${body}`);

        try {
            const users = await User.find();
            return res.status(200).json(users);
        } catch (error) {
            return res.json({ status: 401, message: `Error finding users - ${error}` });
        }
    }

    /**
     * Register a new user in the system. Add a user to the database.
     *
     * @param req
     * @param res
     */
    static async signup(req: Request, res: Response) {
        LOG(`${req.method} ${req.url}`);
        // eslint-disable-next-line object-curly-newline
        const { username, password, email, firstname } = req.body;
        const result = {
            message: '',
            status: 0,
            id: ''
        };
        // create a hash for the password to save it encrypted to the database
        let hash: string;
        try {
            hash = await bcrypt.hash(password, 10);
        } catch (error) {
            LOG(`Error creating the hash: ${error}`);
            result.status = 502;
            result.message = `Error creating Hash: ${error}`;
            return res.status(result.status).json(result);
        }

        const user : IUser = new User({
            username,
            email,
            firstname,
            password: hash
        });
        try {
            const userSaved = await user.save();
            result.status = 200;
            result.message = 'User was saved succesfully';
            result.id = userSaved.id;
            LOG('User saved!!!');
            return res.status(result.status).json(result);
        } catch (error) {
            LOG(`Error saving a user ${error}`);
            result.status = 501;
            result.message = `Error saving user - ${error}`;
            return res.status(result.status).json(result);
        }
    }

    static async login(req: Request, res: Response) {
        LOG(`${req.method} ${req.url}`);
        const { username, password } = req.body;
        const result = {
            status: 0,
            message: '',
            id: '',
            token: ''
        };
        try {
            const userFound = await User.findOne({ username });
            if (userFound == null) {
                result.status = 200;
                result.message = `User not found! - ${username}`;
                return res.status(result.status).json(result);
            }
            const same = await bcrypt.compare(password, userFound.password);
            if (!same) {
                result.status = 201;
                result.message = `Password does not match - ${username}`;
                result.id = '';
            } else {
                result.status = 200;
                result.message = `User found - ${username}`;
                result.id = userFound.id;
                const token = jwt.sign({ username: userFound.username, id: userFound.id }, config.getJwtPrivateKey(), { expiresIn: '1h' });
                result.token = token;
            }
            return res.status(result.status).json(result);
        } catch (error) {
            result.status = 504;
            result.message = `Error Login: in ${error}`;
            LOG(result.message);
            return res.status(result.status).json(result);
        }
    }

    static async chat(req: Request, res: Response) {
        const { stockCode } = req.body;
        let data = '';
        const request = https.get(`https://stooq.com/q/l/?s=${stockCode}&f=sd2t2ohlcv&h&e=csv`, (resp) => {
            resp.on('data', (chunk) => {
                data += chunk;
            });
            resp.on('end', () => {
                csv2json().fromString(data)
                    .then((result) => {
                        console.log(result);
                        let msg = '';
                        for (let i = 0; i < result.length; i += 1) {
                            msg += `${result[i].Symbol} quote is ${result[i].Close} per share.\n`;
                        }
                        res.status(200).json({ message: msg });
                    });
            });
        });
        request.on('error', () => {
            res.status(200).json({ message: 'Something went wrong in the server' });
        });
    }
}
