/* eslint-disable no-undef */
import path from 'path';
import UPSShipment from '../helper/upsshipment';
import readDataFromFile from '../helper/readfile';

test('Sum 1 + 1 should be 2', () => {
    expect(1 + 1).toBe(2);
});

test('readDataFromFile should read a file', () => {
    const filename = path.join(__dirname, '../', 'currier-api-request/rating_ups_ground_request.json');
    const data = readDataFromFile(filename);
    expect(data).toBeDefined();
    // console.log(data);
});

describe('Test UPS class', () => {
    let ups: UPSShipment;
    beforeAll(() => {
        ups = new UPSShipment();
    });
    test('UPS object should be already defined', () => {
        expect(ups).toBeDefined();
    });

    test('UPS params undefined should throw an error', () => {
        // expect(ups.setParams(undefined)).toThrowError('Parameter is undefined');
    });

    test('UPS params should not throw any error', () => {
        ups.setParams({ hello: 'hello' });
        // expect(ups.setParams({ hello: 'hello' })).toBeCalled();
    });
});
