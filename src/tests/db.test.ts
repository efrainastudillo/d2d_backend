/* eslint-disable no-undef */

import Database from '../helper/database';
import config from '../config/config';

describe('Rating Tests', () => {
    let connection: Database;

    beforeAll(() => {
        connection = new Database(
            config.GetDatabaseUser(),
            config.GetDatabasePassword(),
            config.GetDatabaseName()
        );
        connection.connect();
    });

    afterAll(() => {
        console.log('Database is shuting down...');
        connection.shutdown();
    });

    test('insert a user (3+1)=4', () => {
        expect(3 + 1).toBe(4);
    });
});
