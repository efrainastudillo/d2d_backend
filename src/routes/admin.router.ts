import e from 'express';
import UserController from '../controllers/user.controller';

const router: e.Router = e.Router();

router.get('/users', UserController.getUsers);
router.get('/users/:id', UserController.getUserById);

export default router;
