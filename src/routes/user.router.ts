import e from 'express';
import UserController from '../controllers/user.controller';
import auth from '../helper/check-auth';

// eslint-disable-next-line prefer-const
const router: e.Router = e.Router();

router.get('/chat', UserController.chat);
router.get('/users', auth, UserController.getUsers);
router.post('/login', UserController.login);
router.post('/signup', UserController.signup);
router.get('/users/:id', UserController.getUserById);

export default router;
